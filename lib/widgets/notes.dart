import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notally_clone/controller/note_controller.dart';
import 'package:notally_clone/pages/new_note.dart';

import '../model/note_data.dart';

class Notes extends StatefulWidget {
  final NoteData noteData;

  const Notes(this.noteData, {super.key});

  @override
  State<Notes> createState() => _NotesState();
}

class _NotesState extends State<Notes> {
  @override
  Widget build(BuildContext context) {
    final noteData = widget.noteData;
    final controller = Get.put(NoteController());
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              title: Text(noteData.title != '' ? noteData.title : 'Empty note'),
              subtitle: Text(noteData.note),
              onTap: () {
                Get.to(NoteNew(
                  noteData,
                  isNew: false,
                ));
              },
              onLongPress: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return SizedBox(
                        height: 200,
                        child: ListView(
                          padding: const EdgeInsets.all(8.0),
                          children: [
                            TextButton(
                              onPressed: () {
                                controller.remove(noteData.id);
                                Get.back();
                              },
                              style: TextButton.styleFrom(),
                              child: const Align(
                                alignment: Alignment.centerLeft,
                                child: Text('Delete'),
                              ),
                            ),
                          ],
                        ),
                      );
                    });
              },
            )
          ],
        ),
      ),
    );
  }
}
