import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notally_clone/controller/note_controller.dart';
import 'package:notally_clone/util/note_util.dart';

import 'model/note_data.dart';
import 'pages/new_note.dart';
import 'widgets/notes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(NoteController());
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Notally'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.search),
            ),
          ],
        ),
        drawer: const Drawer(
          child: Row(
            children: [],
          ),
        ),
        body: Obx(
          () => Column(
            children: <Widget>[
              for (var noteData in controller.notesData) Notes(noteData),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.to(NoteNew(NoteData(randomString())));
          },
          child: const Icon(Icons.edit),
        ),
      ),
    );
  }
}
