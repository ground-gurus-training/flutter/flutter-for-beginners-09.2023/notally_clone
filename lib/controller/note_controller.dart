import 'package:get/get.dart';
import 'package:notally_clone/util/note_util.dart';

import '../model/note_data.dart';

class NoteController extends GetxController {
  var notesData = <NoteData>[
    NoteData(randomString(),
        title: 'Grocery List', note: 'bread, cheese, milk'),
    NoteData(randomString(),
        title: 'Todo', note: 'create examples for training'),
    NoteData(randomString(),
        title: 'Gaming Backlog', note: 'Baldur\'s Gate 3, Starfield'),
  ].obs;

  find(String id) {
    return notesData.firstWhere((element) => element.id == id);
  }

  remove(String id) {
    notesData.removeWhere((element) => element.id == id);
  }
}
