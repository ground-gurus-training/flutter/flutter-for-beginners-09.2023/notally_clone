import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notally_clone/controller/note_controller.dart';
import 'package:notally_clone/model/note_data.dart';

class NoteNew extends StatelessWidget {
  NoteData noteData;
  bool isNew;
  TextEditingController titleController = TextEditingController();
  TextEditingController noteController = TextEditingController();

  NoteNew(this.noteData, {this.isNew = true, super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(NoteController());
    titleController.text = noteData.title;
    noteController.text = noteData.note;

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              if (isNew) {
                noteData.title = titleController.text;
                noteData.note = noteController.text;
                controller.notesData.add(noteData);
              } else {
                final existingData = controller.find(noteData.id);
                existingData.title = titleController.text;
                existingData.note = noteController.text;
              }
              Get.back();
            },
            icon: const Icon(Icons.save),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: 'Title',
              ),
              controller: titleController,
            ),
            const Divider(),
            TextField(
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: 'Note',
              ),
              controller: noteController,
              maxLength: 512,
              maxLines: 10,
            ),
          ],
        ),
      ),
    );
  }
}
