class NoteData {
  String id;
  String title;
  String note;

  NoteData(this.id, {this.title = '', this.note = ''});

  @override
  String toString() {
    return 'NoteData{id: $id, title: $title, note: $note}';
  }
}
